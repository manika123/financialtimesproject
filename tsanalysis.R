###########################################################
####### TIME SERIES PROCESSING AND ANALYSIS  ##############
####### Author: Manika   ##################################
####### Date: 26-03-2018 ##################################
###########################################################

library(openxlsx)
library(imputeTS)
library(forecast)
library(ggplot2)
library(scales)
set.seed(1234)

#################### Read excel file ######################
data<-read.xlsx("~/financialTimesData/dow jones ft.xlsx", sheet = 1,detectDates = TRUE)
data$Date<-as.Date(data$Date)

######## Insert missing dates and create time series ######
all.dates <- rev(seq(min(as.Date(data$Date)), max(as.Date(data$Date)), by = "1 day"))
all.dates <- data.frame(Date = all.dates)
data<-merge(all.dates, data, by = "Date",all.x = TRUE) 

######## Interpolate and Select relevant time range #######
data$Letzter.Kurs<- na.ma(data$Letzter.Kurs, weighting = "simple")
last.course<-subset(data, Date >='1900-01-01'& Date<='2009-12-31')[,c(-3,-4)]

############# Save Rdata and clear memory #################
save(last.course,file = "~/financialtimesproject/data/dow_jones.Rdata")
save.image()
rm(list = ls())
gc()

########## Split training and test sets (78-32) ###########
load("~/financialtimesproject/data/dow_jones.Rdata")
training.set <- subset(last.course,Date >='1900-01-01'& Date<='1985-12-31')
test.set <- subset(last.course,Date >='1986-01-01'& Date<='2009-12-31')

########### Time Series modeling and forecasting ##########
training.series <- ts(training.set$Letzter.Kurs,start = c(1,1),frequency = 365)
test.series <- ts(test.set$Letzter.Kurs,start = c(1,1),frequency = 365)

# Random Walk
model.stl<-stl(training.series,s.window = 'periodic',robust = TRUE)
forecast.stl<- forecast(model.stl,method = "rwdrift",h=length(test.series))

# Exponential Smoothing
model.holtwinters <- HoltWinters(training.series)
forecast.holtwinters <- forecast(model.holtwinters,h=length(test.series))

model.ets <- ets(training.series)
forecast.ets <- forecast(model.stl,method = "ets",h=length(test.series))

# ARIMA
model.arima <- auto.arima(training.series)
forecast.arima <- forecast(model.arima ,h=length(test.series))

##################### Evaluation ##########################

# Training Set
train.randomwalk <- accuracy(forecast.stl)
train.holtwinters <- accuracy(forecast.holtwinters)
train.ets <- accuracy(forecast.ets)
train.arima <- accuracy(forecast.arima)

# Test Set
test.randomwalk <- accuracy(forecast.stl, test = test.series)
test.holtwinters <- accuracy(forecast.holtwinters,test = test.series)
test.ets <- accuracy(forecast.ets, test = test.series)
test.arima <- accuracy(forecast.arima, test = test.series)

#################### Generate Plot #########################
m <- as.data.frame(cbind(test.set$Date,
           forecast.stl[["mean"]],
           forecast.holtwinters[["mean"]],
           forecast.ets[["mean"]],
           forecast.arima[["mean"]],
           test.set$Letzter.Kurs))
colnames(m)<-c("Date","random.walk","holtwinters","ets","arima","actual.values")

accuracy.rwdrift<-100-test.randomwalk[5]
accuracy.holtwinters<-100-test.holtwinters[5]
accuracy.ets<-100-test.ets[5]
accuracy.arima<-100-test.arima[5]

m$Date <- as.Date(m$Date,origin = "1970-01-01" )
ggplot(data = m, x = Date) +
  geom_line(aes(x = Date, y = random.walk, col = "random.walk")) +
  geom_line(aes(x = Date, y = holtwinters, col = "holtwinters")) +
  geom_line(aes(x = Date, y = ets, col = "ets")) +
  geom_line(aes(x = Date, y = arima, col = "arima")) +
  geom_line(aes(x = Date, y = actual.values, col = "actual.values")) +
  scale_colour_discrete(name = "Group") +
  labs(y = "Values", x = "Date")
